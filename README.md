# qml-fontawesome-5

![License](https://img.shields.io/badge/FontAwesome-5.1.0-orange.svg?longCache=true&style=flat-square) 
![License](https://img.shields.io/badge/License-MIT-yellow.svg?longCache=true&style=flat-square)

Resources for **[FontAwesome 5.1](https://fontawesome.com)** support in **[QML](https://doc.qt.io/qt-5/qtqml-index.html)** applications, based on **[QMLFontAwesome
](https://github.com/qCring/QMLFontAwesome)** by [qCring](https://github.com/qCring)

## Usage
Some resources to get **[FontAwesome](https://fontawesome.com)** up and running in your **[QML](https://doc.qt.io/qt-5/qtqml-index.html)** application real quick. You'll need three simple steps:

### **Copy** the resources into your project

- [Icon.qml](Icon.qml)
- [fontawesome.js](fontawesome.js)

Depending on the icon types you want to use copy one, two or all three of the fonts:

- [fa-solid.ttf](fa-solid.ttf)  
- [fa-regular.ttf](fa-regular.ttf)  
- [fa-brands.ttf](fa-brands.ttf)

Add the files to your resources system.

### **Register** the font(s) in C++ before application startup

```
QFontDatabase::addApplicationFont(:/path/to/fa-solid_regular_or_brands.ttf);
```

### **Access** icons in QML

```
Icon {
    icon: icons.fas_thumbs_up;
}
```

**Solid** icons use fas_, **Regular** icons **far_** and **Brand** icons **fab_** as prefix.

Check out the **[FontAwesome cheatsheet](https://fontawesome.com/cheatsheet/)** or **[Icon Gallery](https://fontawesome.com/icons?d=gallery)** to find individual icon names. By convention, the same names are used as listed in the cheatsheet. The only difference is that dashes are replaced with underscores.
